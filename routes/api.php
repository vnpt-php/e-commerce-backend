<?php

use Illuminate\Http\Request;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\BrandCategoryController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\AddressController;
use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'auth:api',
],
    function() {
        //User routes
        Route::get('/user/current', [UserController::class, 'current']);
        Route::get("/users", [UserController::class, 'users']);
        Route::delete("/user/{id}", [UserController::class, 'remove']);
        Route::post("/user/upload-avatar", [UserController::class, 'uploadAvatar'])->name('upload');

        //Cart routes
        Route::post("/cart/create", [CartController::class, 'insertProductToCart']);
        Route::get("/cart", [CartController::class, 'cart']);
        Route::put("/cart/increase/{id}", [CartController::class, 'increase']);
        Route::put("/cart/decrease/{id}", [CartController::class, 'decrease']);

        //Category routes
        Route::post("/category/create", [CategoryController::class, 'create']);

        //Store routes
        Route::post("/store/create", [StoreController::class, 'create']);
        Route::put("/store/update-address/{id}", [StoreController::class, 'updateAddress']);
        Route::get("/store", [StoreController::class, 'userStore']);

        //Product routes
        Route::post("/product/create", [ProductController::class, 'create'])->name('upload');
        Route::delete("/product/{id}", [ProductController::class, 'removeProduct']);
        Route::get("/my-products", [ProductController::class, 'myProducts']);
        
        //Brand Category routes
        Route::post("/brandCategory/create", [BrandCategoryController::class, 'insertBrandCategory']);
        Route::post("/brand/create-brand-to-category", [BrandController::class, 'createNewBrandToCategory']);
        Route::post("/brand/create", [BrandController::class, 'create']);

        //Order routes
        Route::post("/order/create", [OrderController::class, 'create']);
        Route::get("/order/my-order", [OrderController::class, 'myOrder']);
        Route::get("/order/store-orders", [OrderController::class, 'storeOrder']);
        Route::get("/orders/status={status}", [OrderController::class, 'myOrderWithStatus']);
        Route::delete("/order/{id}", [OrderController::class, 'removeOrder']);
        Route::put("/order/change-status", [OrderController::class, 'changeOrderStatus']);

        //Addresses routes
        Route::post("/address/create", [AddressController::class, 'create']);
        Route::get("/address/{id}", [AddressController::class, 'address']);
        Route::get("/address", [AddressController::class, 'userAddress']);
        Route::put("/address/set-default/{id}", [AddressController::class, 'default']);
        Route::put("/address/update", [AddressController::class, 'update']);
        Route::delete("/address/{id}", [AddressController::class, 'removeAddress']);
    });
Route::get("/stores", [StoreController::class, 'stores']);
Route::get("/store/{id}", [StoreController::class, 'store']);
Route::get("/products", [ProductController::class, 'products']); 
Route::get("/product/{id}", [ProductController::class, 'product']); 
Route::get("/products/category={category_id}", [ProductController::class, 'productsCategory']); 
Route::get("/products/brand={brand_id}", [ProductController::class, 'productsBrand']); 
Route::get("/categories", [CategoryController::class, 'categories']);
Route::get("/brands", [BrandController::class, 'brands']);
Route::get("/image/{id}", [ProductController::class, 'productImages']);
Route::post("/user/create", [UserController::class, 'register']);
Route::post("/user/login", [UserController::class, 'login']);


