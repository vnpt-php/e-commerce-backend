<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Store;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\Services\HashPassword;
use App\Providers\Services\UserService;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Psy\Util\Json;

class UserController extends Controller {

    private $hashPassword;
    private $userService;
    public function __construct(HashPassword $hashPassword, UserService $userService) {
        $this->hashPassword = $hashPassword;
        $this->userService = $userService;
    }

    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8',
            'phone' => ['required', 'regex:/^(03|05|07|08)[0-9]{8}$/']
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => 422,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }
        $user = $this->userService->create($request);
        return response()->json([
            'status' => 200,
            'data' => $user
        ]);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => 422,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        $credentials = $request->only('email', 'password');
        $user = $this->userService->findEmail($credentials['email']);
        if (!$user) {
            return response()->json([
                'status' => 400,
                'error' => 'Email not found!',
            ], 400);
        }
        if (!$this->hashPassword->verify($credentials['password'], $user->password))
        {
            return response()->json([
                'status' => 400,
                'error' => 'Password incorrect!',
            ], 400);
        }

        $token = JWTAuth::fromUser($user);

        return response()->json([
            'status' => 200,
            'data' => [
                'token' => $token,
            ]
        ], 200);
    }

    public function current(Request $request) {
        $userId = $request->user()->id;
        $user = $this->userService->findId($userId);
        return response()->json([
            'status' => 200,
            'data' => $user
        ], 200);
    }

    public function users() {
        $users = $this->userService->fetch();
        return response()->json([
            'status'=>200,
            'data' => $users
        ]);
    }

    public function remove($id) {
        $user = $this->userService->removeUser($id);
        if (!$user) {

        }
        return response()->json([
            'status' => 200,
        ]);
    }

    public function uploadAvatar(Request $request) {
        $userId = $request->user()->id;
        $avatar = $request->file('avatar');
        if(!isset($avatar)) {
            return \response()->json([
                'status' => 400,
                'message' => "Image be Empty!"
            ], 400);
        }
        $changeAvatar = $this->userService->changeAvatar($avatar, $userId);
        return \response()->json([
            'status' => 200,
            'data' => $changeAvatar
        ]);
    }
}
