<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Providers\Services\BrandCategoryService;
use App\Providers\Services\BrandService;
use App\Providers\Services\CategoryService;

class BrandCategoryController extends Controller
{
    private $brandCategoryService, $brandService, $categoryService;
    public function __construct(BrandCategoryService $brandCategoryService, BrandService $brandService, CategoryService $categoryService){
        $this->brandCategoryService = $brandCategoryService;
        $this->brandService = $brandService;
        $this->categoryService = $categoryService;
    }
    
    public function insertBrandCategory(Request $request) {
        $categoryId = $request->all()['category_id'];
        $brands = $request->all()['brands'];
        foreach ($brands as  $brand) { 
            $isBrand = $this->brandService->findId($brand['id']);
            if(!$isBrand) {
                return response()->json([
                    'status' => 404,
                    'message' => "Brand Not Found" . $brand['id'],
                ], 404);
            }
            $isCategory = $this->categoryService->findId($categoryId);
            if(!$isCategory) {
                return response()->json([
                    'status' => 404,
                    'message' => "Category Not Found" . $categoryId,
                ], 404);
            }
            $brandCate = $this->brandCategoryService->insertBrandToCategory($categoryId, $brand['id']);
        }
        return response()->json([
            'status' => 200,
            'message' => "Hi",
            'data' => $request->all(),
        ]);
    }
}
