<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Store;
use App\Providers\Services\StoreService;


class StoreController extends Controller
{

    private $storeService;
    public function __construct(StoreService $storeService) {
        $this->storeService = $storeService;
    }

    public function create(Request $request) {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'address' => 'required|string',
        ]);

        if (!$validator) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
            ], 400);
        }

        $user_store = $this->storeService->findUserId($user->id);
        if ($user_store) {
            return response()->json([
                'status' => 400,
                'message' => "You've got the store!"
            ], 400);
        }
        $request['user_id'] = $user->id;
        $store = $this->storeService->create($request);
        return response()->json([
            'status' => 200,
            'data' => $store
        ]);
    }

    public  function stores() {
        $stores = $this->storeService->fetch();
        return response()->json([
            'status' => 200,
            'data' => $stores 
        ]);
    }

    public function store($id) {
        $store = $this->storeService->findId($id);
        if(!$store) {
            return response()->json([
                'status'=> 404,
                'message' => 'Store not found!'
            ], 404);
        }
        return response()->json([
            'status'=>200,
            'data' => $store
        ]);
    }

    public function updateAddress(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'address' => 'required|string',
        ]);
        if (!$validator) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
            ]);
        }
        $address = $request->all()['address'];
        $store_update = $this->storeService->updateAddress($id, $address );
        return response()->json([
            'status'=> 200,
            'data' => $store_update 
        ]);
    }

    public function userStore(Request $request) {
        $userId = $request->user()->id;
        $store = $this->storeService->findUserId($userId);
        if(!$store) {
            return response()->json([
                'status'=> 404,
                'message' => "You don't have a Store!"
            ], 404);
        }
        return response()->json([
            'status'=> 200,
            'data' => $store
        ]);
    }
}
