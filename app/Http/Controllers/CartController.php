<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\Services\CartService;
use App\Providers\Services\CartItemService;
use App\Providers\Services\ProductService;
use Illuminate\Support\Facades\Validator;


class CartController extends Controller
{
    private $cartService, $cartItemService, $productService;
    public function __construct(CartService $cartService, CartItemService $cartItemService, ProductService $productService){
        $this->cartService = $cartService;
        $this->cartItemService = $cartItemService;
        $this->productService = $productService;
    }

    public function insertProductToCart(Request $request) {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|uuid',
            'quantity' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors(),
                'status' => 422
            ], 422);
        }

        $product = $this->productService->findId($request->product_id);
        if (!$product) {
            return response()->json([
                'status' => 404,
                'message' => "Product not found!"
            ], 404);
        }
        $data = request()->all();  
        $userId = $request->user()->id;
        $storeInCart = $this->cartService->checkStoreInCart($product->store_id, $userId);

        if (!$storeInCart) {
            $cart = $this->cartService->create($product->store_id, $userId);
            if (!$cart) {
                return;
            }
            $data["cart_id"] = $cart->id;
            $cartItem = $this->cartItemService->create($data);
            if (!$cartItem) {
                return;
            }
            return response()->json([
                'message' => "Success!",
                'status' => 200
            ]);
        }

        $productInCartItem = $this->cartItemService->checkProductInCartItem($product->id, $storeInCart->id);
        if ($productInCartItem) {
            $this->cartItemService->increaseQuantity($productInCartItem->id, request()->quantity);
            return response()->json([
                'message' => "success!",
                'status' => 200
            ]);
        }

        $data['cart_id'] = $storeInCart->id;
        $cartItem = $this->cartItemService->create($data);
        if (!$cartItem) {
            return;
        }

        return response()->json([
            'message' => "",
            'status' => 200
        ]);
    }

    public function cart(Request $request)  {
        $userId = $request->user()->id;
        $cart = $this->cartService->cartUserId($userId);

        return response()->json([
            'status' => 200,
            'data' => $cart
        ]);
    }

    public function increase($id) {
        if(!\uuid_is_valid($id)) {
            return response()->json([
                'status' => 400,
                'message' => "UUID inValid!"
            ], 400);
        }

        $cartItem = $this->cartItemService->findId($id);
        if (!$cartItem) {
            return response()->json([
                'status' => 404,
                'message' => "NOT FOUND!"
            ], 404);
        }
        $increase = $this->cartItemService->increaseQuantity($id);
        return response()->json([
            'status' => 200,
            'data' => $increase
        ]);
    }

    public function decrease($id) {
        if(!\uuid_is_valid($id)) {
            return response()->json([
                'status' => 400,
                'message' => "UUID inValid!"
            ], 400);
        }

        $cartItem = $this->cartItemService->findId($id);
        if (!$cartItem) {
            return response()->json([
                'status' => 404,
                'message' => "NOT FOUND!"
            ], 404);
        }
        $decrease = $this->cartItemService->decreaseQuantity($id);
        return response()->json([
            'status' => 200,
            'data' => $decrease
        ]);
    }
}
