<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Providers\Services\CategoryService;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    private $categoryService;
    public function __construct(CategoryService $categoryService ) {
        $this->categoryService = $categoryService;
    }

    public function create(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if (!$validator) {
            return response()->json([
                'errors' => $validator->errors(),
                'status' => 400
            ], 400);
        }

        $findCate = Category::where('name', $request->name)->first();

        if ($findCate) {
            return response()->json([
                'status' => 400,
                'message' => 'Category already exists!',
            ], 400);
        }

        $category = Category::create([
            'id' => \uuid_create(),
            'name' => $request->name
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'Category created successfully',
            'data' => $category
        ]);
    }

    public function categories() {
        $categories = Category::with('brands')->get();
        return response()->json([
            'status' => 200,
            'data' => $categories
        ]);
    }
}
