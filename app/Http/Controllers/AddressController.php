<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Providers\Services\AddressService;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    private $addressService;
    public function __construct(AddressService $addressService) {
        $this->addressService = $addressService;
    }

    public function create(Request $request) {
        $address = $request->all();
        $userId = $request->user()->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'ward' => 'required|string',
            'city' => 'required|string',
            'district' => 'required|string',
            'detail' => 'required|string',
            'phone' => ['required', 'regex:/^(03|05|07|08)[0-9]{8}$/']
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => 422,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }

        $address['user_id'] = $userId;
        $newAddress = $this->addressService->create($address);

        return response()->json([
            'status' => 200,
            'address' => $newAddress,
        ]);
    }

    public function removeAddress($id) {
        $address = $this->addressService->remove($id);
        return response()->json([
            'status' => 200,
            'message' => $address
        ]);
    }

    public function address($id) {
        $address = $this->addressService->findId($id);
        if(!$address) {
            return response()->json([
                'status' => 404,
                'message' => "Address not found!"
            ], 404);
        }
        return response()->json([
            'status' => 200,
            'data' => $address
        ]);
    }


    public function userAddress(Request $request) {
        $useId = $request->user()->id;
        $address = $this->addressService->findUserId($useId);
        return response()->json([
            'status' => 200,
            'data' => $address
        ]);
    }

    public function default(Request $request, $id) {
        $userId = $request->user()->id;
        $default = $this->addressService->setDefault($id, $userId);
        return response()->json([
            'status' => 200,
            'data' => $default
        ]);
    }

    public function update(Request $request) {
        $address = $request->all();
        $userId = $request->user()->id;
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'ward' => 'required|string',
            'city' => 'required|string',
            'district' => 'required|string',
            'detail' => 'required|string',
            'phone' => ['required', 'regex:/^(03|05|07|08)[0-9]{8}$/']
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success' => 422,
                'message' => 'Validation Error',
                'errors' => $validator->errors()
            ], 422);
        }
        $update = $this->addressService->update($address, $userId);
        return response()->json([
            'status' => 200,
            'data' => $update
        ]);
    }
}
