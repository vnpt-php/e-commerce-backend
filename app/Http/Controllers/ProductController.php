<?php

namespace App\Http\Controllers;

use Cloudinary;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Support\Facades\Validator;
use App\Models\Store;
use App\Providers\Services\ProductService;
use App\Providers\Services\StoreService;
use App\Providers\Services\BrandCategoryService;
use App\Providers\Services\ProductImageService;
use App\Providers\Services\MediaService;

class ProductController extends Controller
{

    private $productSerivce, $storeService, $brandCategoryService, $productImageService, $mediaService;
    public function __construct(ProductService $productService, StoreService $storeService, BrandCategoryService $brandCategoryService, ProductImageService $productImageService, MediaService $mediaService) {
        $this->productSerivce = $productService;
        $this->storeService = $storeService;
        $this->brandCategoryService = $brandCategoryService;
        $this->productImageService = $productImageService;
        $this->mediaService = $mediaService;
    }

    public function create(Request $request) {
        $data = json_decode($request->input('data'), true);
        $uploadFiles = $request->file('images');
        $validator = Validator::make([
            'images' => $request->file('images'),
            'data' => $data
        ], [
            'images.*' => 'required|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'data.*' => 'required',
            'data.name' => 'required|string',
            'data.description' => 'required|string',
            'data.price' => 'required|numeric',
            'data.brand_id' => 'required|uuid',
            'data.category_id' => 'required|uuid'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $brandCategory = $this->brandCategoryService->checkBrandCategory($data['category_id'], $data['brand_id']);
        if(!$brandCategory) {
            return response()->json([
                'status' => 400,
                'message' => "Brand does't exist in Category!"
            ],400);
        }

        $userId = $request->user()->id;
        $storeId = $this->storeService->findUserId($userId)->id;
        if(!$storeId) {
            return response()->json([
                'status' => 400,
                'error' => "You don't have Store!"
            ], 400);
        }

        if(!$uploadFiles) {
            return response()->json([
                'status' => 400,
                'error' => "Product image empty!"
            ], 400);
        }
        $data['store_id'] = $storeId;

        $product = $this->productSerivce->create($data);

        foreach($uploadFiles as $image) {
            $imageUpload = $this->mediaService->uploadImage($image);
            $productImage = $this->productImageService->create($product->id, $imageUpload->getSecurePath(), $imageUpload->getPublicId());
        }
        
        return response()->json([
            'status' => 200,
            'message' => "Created new producted!",
            'data' => $product,
        ]);
    }

    public function products() {
        $products = Product::with('productImages', 'brand', 'category', 'store')->get();
        return response()->json([
            'status' => 200,
            'data' => $products
        ]);
    }

    public function product($id) {
        $product = $this->productSerivce->findId($id);
        return response()->json([
            'status' => 200,
            'data' => $product
        ]);
    }

    public function productImages($id) {
        if(!\uuid_is_valid($id)) return \response()->json(['status' => 400 ,'message' => "UUID inValid!"], 400); 
        $images = $this->productImageService->productImage($id);
        return response()->json([
            'status' => 200,
            'data' => $images
        ]);
    }

    public function removeProduct($id) {
        if(!\uuid_is_valid($id)) return \response()->json(['status' => 400 ,'message' => "UUID inValid!"], 400); 
        $find = $this->productSerivce->findId($id);
        if(!$find) return \response()->json(['status' => 404 ,'message' => "Product not found!"], 404); 
        $remove = $this->productSerivce->remove($id);
        if(!$remove) {
            return response()->json([
                'status' => 400,
                'message' => 'remove failed'
            ], 400);
        }
        return response()->json([
            'status' => 200,
            'message' => $remove
        ]);
    }

    public function productsCategory($category_id) {
        $products = $this->productSerivce->productsCate($category_id);
        return response()->json([
            'status' => 200,
            'data' => $products
        ]);
    }

    public function productsBrand($brand_id) {
        $products = $this->productSerivce->productsBrand($brand_id);
        return response()->json([
            'status' => 200,
            'data' => $products
        ]);
    }

    public function myProducts(Request $request) {
        $userId = $request->user()->id;
        $store = $this->storeService->findUserId($userId);
        if(!$store) {
            return response()->json([
                'status' => 404,
                'message' => "STORE NOT FOUND!"
            ], 404);
        }
        $products = $this->productSerivce->productsStore($store['id']);
        return response()->json([
            'status' => 200,
            'data' => $products
        ]);
    }

    public function storeProducts(Request $request) {
        $userId = $request->user()->id;

    }
}
