<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\BrandCategory;
use Illuminate\Support\Facades\Validator;
use App\Providers\Services\BrandService;
use App\Providers\Services\BrandCategoryService;

class BrandController extends Controller
{
    private $brandService, $brandCategoryService;
    public function __construct(BrandService $brandService, BrandCategoryService $brandCategoryService) {
        $this->brandService = $brandService;
        $this->brandCategoryService = $brandCategoryService;
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $categories = $request->categories;

        if (!$validator) {
            return response()->json([
                'errors' => $validator->errors(),
                'status' => 400
            ], 400);
        }

        $checkBrand = Brand::where('name', $request->name)->first();
        if ($checkBrand) {
            return response()->json([
                'status' => 400,
                'message' => 'Brand already exists!'
            ], 400);
        }

        $brand = Brand::create([
            'id' => \uuid_create(),
            'name' => $request->name
        ]);

        if(count($categories) > 0) {
            foreach ($categories as $category ) {
                $brandCategory = BrandCategory::create([
                    "id" => \uuid_create(),
                    "brand_id" => $brand->id,
                    "category_id" => $category['id']
                ]);
            }
        }

        return response()->json([
            'status' => 200,
            'message' => "Created new brand!",
            'data' => $categories
        ]);
    }

    public function brands() {
        $brands = Brand::with('categories')->get();
        return response()->json([
            "status" => 200,
            "message" => "Brands",
            "data" => $brands
        ]);
    }

    public function createNewBrandToCategory(Request $request) {
        $brands = $request->all()['brands'];
        $categoryId = $request->all()['category_id'];
        
        if (count($brands) == 0) {
            return response()->json([
                'status' => 400,
                'message' => "Requested required brand name!"
            ], 400);
        }

        foreach($brands as $brand) {
            $isBranch = $this->brandService->findName($brand['name']);
            if($isBranch) {
                $isBrandCate = $this->brandCategoryService->checkBrandCategory($categoryId, $isBranch['id']);
                if(!$isBrandCate) {
                    $this->brandCategoryService->insertBrandToCategory($isBranch['id'], $categoryId);
                }
            } else {
                $newBrand = $this->brandService->create($brand['name']);
                if($newBrand) {
                    $brandCate = $this->brandCategoryService->insertBrandToCategory($newBrand->id, $categoryId);
                }
            }
        }
        return response()->json([
            'status' => 200,
            'message' => "Successfully created"
        ]);
    }
}
