<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Providers\Services\OrderService;
use App\Providers\Services\OrderItemService;
use App\Providers\Services\ProductService;
use App\Providers\Services\StoreService;
use App\Providers\Services\CartItemService;
use App\Providers\Services\CartService;
use Illuminate\Support\Facades\Validator;


class OrderController extends Controller
{
    private $orderService, $orderItemService, $productService, $storeService, $cartItemService, $cartService;
    public function __construct(CartService $cartService, CartItemService $cartItemService, OrderService $orderService, OrderItemService $orderItemService, ProductService $productService, StoreService $storeService){
        $this->orderService = $orderService;
        $this->orderItemService = $orderItemService;
        $this->productService = $productService;
        $this->storeService = $storeService;
        $this->cartItemService = $cartItemService; 
        $this->cartService = $cartService; 
    }

    public function create(Request $request) {
        $userId = $request->user()->id;
        $orders = $request->all()["orders"];

        if(count($orders) == 0 ) {
            return response()->json([
                'status' => 400,
                'error' => "Order be empty"
            ]);
        }

        foreach($orders as $order) {
            $store_id = $order["store_id"];
            $address_id = $order["address_id"];
            if(count($order['products']) == 0) {
                return response()->json([
                    'status' => 400,
                    'error' => "Products in Order be Empty!"
                ]);
            }
            foreach($order['products'] as $product ) {
                $pd = $this->productService->findId($product['id']);
                $store = $this->storeService->findId($store_id);
                if($pd && $store) {
                    $order = $this->orderService->create($userId, $store_id, $address_id);
                    if($order) $orderItem = $this->orderItemService->create($order['id'], $pd['id'], $product['quantity']);
                    $cartItem = $this->cartItemService->findId($product['cart_item_id']);
                    if($cartItem) {
                        $cart = $this->cartService->findId($cartItem->cart_id);
                        if(count($cart['cartItems']) <= 1) {
                            $cart->delete();
                        } else {
                            $cartItem->delete();
                        }
                    }
                }
            }
        }
        return response()->json([
            'status' => 200,
            'message' => "Success!"
        ]);
    }

    public function findId($order_id) {
        $order = Order::find($order_id);
        return $order;
    }

    public function myOrder(Request $request) {
        $userId = $request->user()->id;
        $orders = $this->orderService->findUserId($userId);
        return response()->json([
            'status' => 200,
            'data' => $orders
        ]);
    }

    public function myOrderWithStatus(Request $request, $status) {
        $userId = $request->user()->id;
        if($status != 'delivering' && $status != 'delivered' && $status != 'canceled' && $status != 'ordered') {
            return response()->json([
                'status' => 400,
                'message' => "Status not match type!"
            ], 400);
        }
        $orders = $this->orderService->orderStatus($status, $userId);
        return response()->json([
            'status' => 200,
            'data' => $orders
        ]);
    }

    public function changeOrderStatus(Request $request) {
        $status = $request->all()['status'];
        $id = $request->all()['id'];
        if(!\uuid_is_valid($id)) {
            return response()->json([
                'status' => 400,
                'message' => "UUID inValid!"
            ], 400);
        }
        if($status != 'delivering' && $status != 'delivered' && $status != 'canceled' && $status != 'ordered') {
            return response()->json([
                'status' => 400,
                'message' => "Status not match type!"
            ], 400);
        }
        $checkOrder = $this->orderService->findId($id);
        if($status == 'canceled' && $checkOrder['delivering']) {
            return response()->json([
                'status' => 400,
                'message' => "Order is delivering!"
            ], 400);
        }
        $order = $this->orderService->changeOrderStatus($id, $status);
        return response()->json([
            'status' => 200,
            'data' => $order
        ]);
    }

    public function removeOrder($id) {
        if(!\uuid_is_valid($id)) {
            return response()->json([
                'status' => 400,
                'message' => "UUID inValid!"
            ], 400);
        }

        $removeOrder = $this->orderService->remove($id);
        return response()->json([
            'status' => 200,
            'data' => $removeOrder
        ]);

    }

    public function storeOrder(Request $request) {
        $userId = $request->user()->id;
        $store = $this->storeService->findUserId($userId);
        if(!$store) {
            return response()->json([
                'status' => 404,
                'message' => "User don't have store!"
            ], 404);
        }

        $orders = $this->orderService->findOrderStoreId($store['id']);
        return response()->json([
            'status' => 200,
            'data' => $orders
        ]);
    }
}
