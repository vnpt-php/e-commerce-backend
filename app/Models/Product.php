<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductImage;
use App\Models\CartItem;
use App\Models\Brand;
use App\Models\Category;
use App\Models\OrderItem;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    public $primaryKey = 'id';
    protected $keyType = "string";


    protected $fillable = [
        'id',
        'name',
        'price',
        'description',
        'store_id',
        'category_id',
        'brand_id',
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public function store() {
        return $this->belongsTo(Store::class);
    }

    public function productImages() {
        return $this->hasMany(ProductImage::class);
    }

    public function cartItems() {
        return $this->hasMany(CartItem::class);
    }

    public function category() {
        return $this->belongsTo(Category::class);
    }
    
    public function brand() {
        return $this->belongsTo(Brand::class);
    }

    public function orderItems() {
        return $this->hasMany(OrderItem::class);
    }
    
}
