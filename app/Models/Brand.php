<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;

class Brand extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    protected $primayKey = 'id';
    protected $table = "brands";

    protected $fillable = [
        'id',
        'name'
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public function categories( ) {
        return $this->belongsToMany(Category::class);
    }
}
