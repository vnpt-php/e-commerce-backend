<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;

    protected $table = "product_images";
    protected $keyType = "string";
    public $primaryKey = 'id';


    protected $fillable = [
        'id',
        'product_id',
        'url',
        'public_id',
    ];

    protected $casts = [
        "id" => 'string',
        'product_id' => 'string',
    ];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
