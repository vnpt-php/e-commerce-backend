<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Order;

class Address extends Model
{
    use HasFactory;
    protected $keyType = 'string';
    protected $primayKey = 'id';
    protected $table = "address";

    protected $fillable = [
        'id',
        'user_id' ,
        'name' ,
        'ward' ,
        'district',
        'city',
        'phone' ,
        'default' ,
        'detail' 
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public function user( ) {
        return $this->belongsToMany(User::class);
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
