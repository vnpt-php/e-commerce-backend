<?php
namespace App\Models;

use App\Models\Store;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject{
    use HasFactory;
    use Notifiable;

    protected $keyType = "string";
    protected $table = 'users';
    public $primaryKey = 'id';


    protected $fillable = [
        'id',
        'name',
        'email',
        'phone',
        'store',
        'password',
        'avatar',
        'public_id'
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public function store()
    {
        return $this->hasOne(Store::class);
    }

    public function address() {
        return $this->hasMany(Address::class);
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }

    public function carts() {
        return $this->hasMany(Cart::class);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

}