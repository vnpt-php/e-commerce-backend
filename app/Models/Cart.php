<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Store;
use App\Models\CartItem;

class Cart extends Model
{
    use HasFactory;
    protected $table = "carts";
    protected $keyType = "string";
    protected $primayKey = 'id';
    
    protected $fillable = [
        'id',
        'user_id',
        'store_id',
    ];
    protected $casts = [
        'id' => 'string'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function cartItems() {
        return $this->hasMany(CartItem::class);
    }

    public function store() {
        return $this->belongsTo(Store::class);
    }
}
