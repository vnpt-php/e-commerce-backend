<?php
namespace App\Models;
use App\Models\User;
use App\Models\Cart;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Store extends Model {

    use HasFactory;
    protected $keyType = "string";
    protected $table = 'stores';
    public $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'user_id',
        'address'
    ];

    protected $casts =[
        'id' => 'string',
        'user_id' => 'string',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

    public function carts() {
        return $this->hasMany(Cart::class);
    }

    public function orders() {
        return $this->hasMany(Order::class);
    }
}