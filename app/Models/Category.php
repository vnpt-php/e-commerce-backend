<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Brand;
use App\Models\Product;

class Category extends Model
{
    use HasFactory;

    protected $keyType = 'string';
    protected $primayKey = 'id';
    protected $table = "categories";

    protected $fillable = [
        'id',
        'name'
    ];

    protected $casts = [
        'id' => 'string'
    ];

    public function brands() {
        return $this->belongsToMany(Brand::class);
    }

    public function products() {
        return $this->hasMany(Product::class);
    }

}
