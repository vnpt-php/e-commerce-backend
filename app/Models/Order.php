<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Store;
use App\Models\Address;
use App\Models\OrderItem;

class Order extends Model
{
    use HasFactory;
    protected $table = "orders";
    protected $keyType = "string";
    protected $primayKey = 'id';
    
    protected $fillable = [
        'id',
        'user_id',
        'store_id',
        'status',
        'address_id'
    ];
    protected $casts = [
        'id' => 'string'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function orderItems(){
        return $this->hasMany(OrderItem::class);
    }

    public function store() {
        return $this->belongsTo(Store::class);
    }

    public function address() {
        return $this->belongsTo(Address::class);
    }
}
