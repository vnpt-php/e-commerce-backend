<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Response;
use Illuminate\Auth\Access\Response as AccessResponse;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        $this->registerPolicies();
        
        Gate::define('update-post', function ($user, $post) {
            if ($user->id === $post->user_id) {
                return true;
            }
            
            return $this->unauthorized();
        });
    }

    protected function unauthorized($message = 'Unauthorized')
    {
        if (request()->expectsJson()) {
            return Response::json(['message' => $message], 401);
        }

        throw new AuthorizationException($message);
    }
}
