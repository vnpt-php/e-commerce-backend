<?php
namespace App\Providers\Services;
use App\Models\OrderItem;

class OrderItemService {
    public function create($order_id, $product_id, $quantity = 1){
        $orderItem = OrderItem::create([
            'id' => \uuid_create(),
            'order_id' => $order_id,
            'product_id' => $product_id,
            'quantity' => $quantity,
        ]);
        return $orderItem;
    }
}