<?php
namespace App\Providers\Services;

use App\Models\Address;

class AddressService {
    public function create($address) {
        if (!isset($address['default'])) {
            $address['default'] = false;
        }
        $address = Address::create([
            'id' => \uuid_create(),
            'user_id' => $address['user_id'],
            'name' => $address['name'],
            'ward' => $address['ward'],
            'district' => $address['district'],
            'city' => $address['city'],
            'phone' => $address['phone'],
            'default' => $address['default'],
            'detail' => $address['detail']
        ]);
        return $address;
    }

    public function setDefault($id, $userId) {
        $address = Address::where('default', true)->where('user_id', $userId)->first();
        if($address) {
            $address->update([
                'default' => false
            ]);
        }
        $default = Address::find($id);
        if($default)  return $default->update(['default' => true]);
    }

    public function remove($id) {
        $address = Address::find($id);
        return $address->delete();
    }

    public function findId($id) {
        $address = Address::find($id)->first();
        return $address;
    }

    public function findUserId($id) {
        $address = Address::where("user_id", $id)->get();
        return $address;
    }

    public function update($address, $userId) {
        if($address['default']){
            $findDefault = Address::where('user_id', $userId)->where('default', true)->first();
            if($findDefault) $findDefault->update(['default' => false]); 
        }
        $update = Address::where('id', $address['id'])->first();
        return $update->update([
            'name' => $address['name'],
            'ward' => $address['ward'],
            'district' => $address['district'],
            'city' => $address['city'],
            'phone' => $address['phone'],
            'default' => $address['default'],
            'detail' => $address['detail']
        ]);
    }
}