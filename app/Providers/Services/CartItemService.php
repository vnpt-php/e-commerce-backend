<?php
namespace App\Providers\Services;
use App\Models\CartItem;
use App\Providers\Services\CartService;

class CartItemService {

    private $cartService;
    public function __construct(CartService $cartService ) {
        $this->cartService = $cartService;
    }

    public function create($data) {
        $cartItem = CartItem::create([
            'id' => \uuid_create(),
            'cart_id' => $data['cart_id'],
            'product_id' => $data['product_id'],
            'quantity' => $data['quantity']
        ]);
        return $cartItem;
    }

    public function checkProductInCartItem($product_id, $cart_id) {
        $cartItem = CartItem::where('product_id', $product_id)->where('cart_id', $cart_id)->first();
        return $cartItem;
    }

    public function increaseQuantity($id, $quantity = 1) {
        $increase = CartItem::findOrFail($id);
        $increase->quantity = $increase->quantity + $quantity;
        $increase->save();
        return $increase;
    }

    public function decreaseQuantity($id, $quantity = 1) {
        $decrease = CartItem::findOrFail($id);
        $cart = $this->cartService->findId($decrease->cart_id);
        if($decrease->quantity <= $quantity) {
            if(count($cart->cartItems) <= $quantity) {
                return $cart->delete();
            }
            return $decrease->delete();
        }
        $decrease->quantity = $decrease->quantity - $quantity;
        $decrease->save();
        return $decrease;
    }

    public function findId($id) {
        $cartItem = CartItem::find($id);
        return $cartItem;
    }
}