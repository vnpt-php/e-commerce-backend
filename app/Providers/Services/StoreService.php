<?php

namespace App\Providers\Services;
use App\Models\Store;

class StoreService {

    public function findId($id) {
        return Store::find($id);
    }

    public function create($store) {
        return Store::create([
            'id' => \uuid_create(),
            'name' => $store->name,
            'user_id' => $store->user_id,
            'address' => $store->address
        ]);
    }

    public function removeStore($id) {
        $store = Store::find($id);
        return $store->delete();
    }

    public function findUserId($user_id) {
        $store = Store::where('user_id', $user_id)->first();
        return $store;
    }
    public function fetch() {
        $stores = Store::with("user")->get();
        foreach ($stores as $store) {
            unset($store->user['password']);
        }
        return $stores; 
    }

    public function updateAddress($id, $address) {
        $store = Store::find($id);
        return $store->update([
            'address' => $address
        ]);
    }
}