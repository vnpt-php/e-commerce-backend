<?php
namespace App\Providers\Services;
use Cloudinary;

class MediaService {
    public function uploadImage($file) {
        // print_r ($file->);
        $image = Cloudinary::upload($file->getRealPath());
        return $image;
    }

    public function removeImage($public_id) {
        $remove = Cloudinary::destroy($public_id);
        return $remove;
    }
}