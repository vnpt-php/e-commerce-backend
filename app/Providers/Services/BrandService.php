<?php

namespace App\Providers\Services;
use App\Models\Brand;

class BrandService {
    public function findId($id) {
        $brand = Brand::find($id);
        return $brand;
    }

    public function create($name) {
        $brand = Brand::create([
            "id" => \uuid_create(),
            "name" => $name,
        ]);
        return $brand;
    }

    public function findName($name) {
        $brand = Brand::where('name', $name)->first();
        return $brand;
    }
}