<?php
namespace App\Providers\Services;
use App\Models\BrandCategory;

class BrandCategoryService {
    public function checkBrandCategory($category_id, $brand_id) {
        $brandCategory = BrandCategory::where('category_id', $category_id)
            ->where('brand_id', $brand_id)
            ->exists();
        return $brandCategory;
    }

    public function insertBrandToCategory($brand_id, $category_id) {
        $brandCategory = BrandCategory::create([
            'id' => \uuid_create(),
            'brand_id' => $brand_id,
            'category_id' => $category_id
        ]);
        return $brandCategory;
    }
}
