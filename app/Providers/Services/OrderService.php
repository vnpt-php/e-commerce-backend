<?php
namespace App\Providers\Services;
use App\Models\Order;

class OrderService {
    public function findId($id){
        $order = Order::find($id);
        return $order;
    }

    public function create($user_id, $store_id, $address_id){
        $order = Order::create([
            'id' => \uuid_create(),
            'user_id' => $user_id,
            'store_id' => $store_id,
            'status' => 'ordered',
            'address_id' => $address_id
        ]);
        return $order;
    }

    public function findUserId($user_id) {
        $orders = Order::with('user', 'orderItems', 'store', 'orderItems.product', 'orderItems.product.productImages', 'address')->where('user_id', $user_id)->get();
        return $orders;
    }

    public function findOrderStoreId($store_id) {
        $orders = Order::with('user', 'orderItems', 'store', 'orderItems.product', 'address')->where('store_id', $store_id)->get();
        return $orders;
    }

    public function orderStatus($status, $userId) {
        $orders = Order::with('user', 'store', 'orderItems.product', 'orderItems.product.productImages', 'address')->where('status', $status)->where('user_id', $userId)->get();
        return $orders;
    }

    public function changeOrderStatus($orderId, $status){
        $order = Order::find($orderId);
        return $order->update([
            'status' => $status
        ]);
    }

    public function remove($id) {
        $order = Order::find($id);
        return $order->delete();
    }
}