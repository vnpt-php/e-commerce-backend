<?php
namespace App\Providers\Services;
use App\Providers\Services\MediaService;
use App\Providers\Services\ProductImageService;
use App\Models\Product;

class ProductService {
    private $mediaService, $productImageService;
    public function __construct(MediaService $mediaService,ProductImageService $productImageService ) {
        $this->mediaService = $mediaService;
        $this->productImageService = $productImageService;
    }

    public function create($data) {
        $product = Product::create([
            'id' => \uuid_create(),
            'name' => $data['name'],
            'description' => $data['description'],
            'price' => $data['price'],
            'store_id' => $data['store_id'],
            'brand_id' => $data['brand_id'],
            'category_id' => $data['category_id']
        ]);

        return $product;
    }

    public function findId($id) {
        $product = Product::with("productImages", "category", "store", "brand")->find($id);
        return $product;
    }

    public function remove($id) {
        $product = $this->findId($id);
        foreach($product['productImages'] as $image) {
            $this->mediaService->removeImage($image['public_id']);
        }
        return $product->delete();
    }

    public function productsCate($cate_id) {
        $products = Product::with("productImages", "category", "store", "brand")->where('category_id', $cate_id )->get();
        return $products;
    }

    public function productsBrand($brand_id) {
        $products = Product::with("productImages", "category", "store", "brand")->where('brand_id', $brand_id )->get();
        return $products;
    }

    public function productsStore($store_id) {
        $products = Product::with("productImages", "category", "store", "brand")->where('store_id', $store_id )->get();
        return $products;
    }
}