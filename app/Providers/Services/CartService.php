<?php
namespace App\Providers\Services;
use App\Models\Cart;

class CartService {
    
    public function create($store_id, $user_id) {
        $cart = Cart::create([
            'id' => \uuid_create(),
            'store_id' => $store_id,
            'user_id' => $user_id,
        ]);
        return $cart;
    }

    public function checkStoreInCart($store_id, $user_id) {
        $storeCart = Cart::where('store_id', $store_id)->where('user_id', $user_id)->first();
        return $storeCart;
    }

    public function cartUserId($userId) {
        $cart = Cart::with("cartItems.product", 'store')->where('user_id', $userId)->get();
        return $cart;
    }

    public function findId($id) {
        $cart = Cart::with("cartItems")->find($id);
        return $cart;
    }

    public function removeCart($id) {
        $cart = Cart::find($id);
        return $cart->delete();
    }
}