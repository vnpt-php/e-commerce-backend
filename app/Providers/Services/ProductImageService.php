<?php
namespace App\Providers\Services;
use App\Models\ProductImage;
class ProductImageService {
    public function create($product_id, $url, $public_id) {
        $image = ProductImage::create([
            'id' => \uuid_create(),
            'product_id' => $product_id,
            'url' => $url,
            'public_id' => $public_id,
        ]);
        return $image;
    }

    public function productImage($product_id) {
        $images = ProductImage::where('product_id', $product_id)->get();
        return $images;
    }
}