<?php
namespace App\Providers\Services;
use App\Models\Category;
class CategoryService {
    public function findId($id) {
        $category = Category::find($id);
        return $category;
    }
}