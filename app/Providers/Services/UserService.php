<?php
namespace App\Providers\Services;
use App\Models\User;
use App\Providers\Services\HashPassword;
use App\Providers\Services\MediaService;
class UserService {

    private $hashPassword, $mediaService;
    public function __construct(HashPassword $hashPassword, MediaService $mediaService){
        $this->hashPassword = $hashPassword;
        $this->mediaService = $mediaService;
    }
    public function findId($id) {
        $user =  User::with('carts', 'store', 'address')->find($id);
        unset($user['password']);
        return $user;
    }

    public function findEmail($email) {
        return User::where('email', $email)->first();
    }

    public function fetch() {
        $users = User::with("store")->get();
        foreach ($users as $user) {
            unset($user['password']);
        }
        return $users;
    }

    public function removeUser($id) {
        $user = User::find($id);
        return $user->delete();
    }

    public function create($user) {
        $user = User::create([
            'id' => uuid_create(),
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->phone,
            'password' => $this->hashPassword->hash($user->password),
            'avatar' => $user->avatar,
        ]);
        unset($user['password']);
        return $user;
    }

    public function changeAvatar($file, $userId) {
        $user = User::find($userId);
        if($user && $file) {
            $upload = $this->mediaService->uploadImage($file);
            if($user->public_id) $remove = $this->mediaService->removeImage($user['public_id']);
            return $user->update([
                'avatar' => $upload->getSecurePath(),
                'public_id' => $upload->getPublicId(),
            ]);
        }
        return false;
    }
}