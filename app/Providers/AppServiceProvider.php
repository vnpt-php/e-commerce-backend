<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Providers\Services\HashPassword;
use App\Providers\Services\UserSerice;
use App\Providers\Services\StoreService;
use App\Providers\Services\ProductService;
use App\Providers\Services\MediaService;
use App\Providers\Services\ProductImageService;
use App\Providers\Services\BrandCategoryService;
use App\Providers\Services\CartService;
use App\Providers\Services\BrandService;
use App\Providers\Services\CategoryService;
use App\Providers\Services\OrderService;
use App\Providers\Services\OrderItemService;
use App\Providers\Services\AddressService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind('hashpassword', function () {
            return new HashPassword();
        });

        $this->app->bind('userService', function() {
            return new UserSerice();
        });

        $this->app->bind('storeService', function() {
            return new StoreService();
        });

        $this->app->bind('productService', function() {
            return new ProductService();
        });

        $this->app->bind('mediaService', function() {
            return new MediaService();
        });

        $this->app->bind('productImageService', function() {
            return new MediaService();
        });
        
        $this->app->bind('brandCategoryService', function() {
            return new BrandCategoryService();
        });

        $this->app->bind('cartService', function() {
            return new CartService();
        });

        $this->app->bind('brandService', function() {
            return new BrandService();
        });

        $this->app->bind('categoryService', function() {
            return new CategoryService();
        });

        $this->app->bind('orderService', function() {
            return new CategoryService();
        });

        $this->app->bind('orderItemService', function() {
            return new OrderItemService();
        });

        $this->app->bind('addressService', function() {
            return new AddressService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
